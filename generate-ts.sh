#!/bin/bash
set -e

echo "Generating typescript file ..."
(cd server && cargo run --bin generate-ts) \
  | sed --expression 's/export //g' \
  > client/server.d.ts

echo "Type checking generated file ..."
(cd client && npx tsc --noEmit ./server.d.ts)

echo "Formatting file ..."
(cd client && npx prettier --write ./server.d.ts)

echo "Done!"
