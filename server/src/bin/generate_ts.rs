fn main() {
    #[cfg(debug_assertions)]
    {
        use typescript_definitions::TypeScriptifyTrait;

        println!("{}\n", lib::PlayerId::type_script_ify());
        println!("{}\n", lib::GameSettings::type_script_ify());
        println!("{}\n", lib::GameState::type_script_ify());
        println!("{}\n", lib::Player::type_script_ify());

        println!("export type Card =");
        for card in lib::Card::cards() {
            println!("  | {}", card as u8);
        }

        println!("\n{}", lib::Phase::type_script_ify());
        println!("\n{}", lib::Action::type_script_ify());
    }
}
