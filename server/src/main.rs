mod dto;

use actix_web::{post, web, App, HttpResponse, HttpServer, Responder};

use crate::dto::InfoRes;

#[post("/info")]
async fn info(_req: web::Json<dto::InfoReq>) -> impl Responder {
    HttpResponse::Ok().json(InfoRes {
        player_counts: vec![2],
    })
}

#[post("/initial-state")]
async fn initial_state(req: web::Json<dto::InitialStateReq>) -> impl Responder {
    match lib::GameState::create(&req.players, &req.settings, req.seed) {
        Some(state) => HttpResponse::Ok().json(dto::InitialStateRes { state }),
        None => HttpResponse::UnprocessableEntity().finish(),
    }
}

#[post("/perform-action")]
async fn perform_action(req: web::Json<dto::PerformActionReq>) -> impl Responder {
    match req
        .state
        .perform_action(&req.settings, req.performed_by, &req.action, req.seed)
    {
        Some(next_state) => HttpResponse::Ok().json(dto::PerformActionRes {
            completed: next_state.is_game_completed(),
            next_state,
        }),
        None => HttpResponse::UnprocessableEntity().finish(),
    }
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    let port: u16 = std::env::var("PORT").unwrap().parse().unwrap();

    println!("listening on port {}", port);
    HttpServer::new(|| {
        App::new()
            .service(info)
            .service(initial_state)
            .service(perform_action)
    })
    .bind(format!("0.0.0.0:{}", port))?
    .run()
    .await
}
