use rand::prelude::*;
use serde::{de, Deserialize, Serialize};
use serde_repr::{Deserialize_repr, Serialize_repr};
use std::collections::{HashMap, HashSet};
use std::convert::TryFrom;
use std::fmt;
use typescript_definitions::TypeScriptify;

#[derive(Debug, Clone, Copy, TypeScriptify, Serialize, Hash, Eq, PartialEq)]
pub struct PlayerId(u32);

impl<'de> Deserialize<'de> for PlayerId {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: de::Deserializer<'de>,
    {
        struct PlayerIdVisitor;

        impl<'de> de::Visitor<'de> for PlayerIdVisitor {
            type Value = PlayerId;

            fn expecting(&self, f: &mut fmt::Formatter) -> fmt::Result {
                f.write_str("player id as a u32 or stringified u32")
            }

            fn visit_u64<E>(self, id: u64) -> Result<Self::Value, E>
            where
                E: de::Error,
            {
                u32::try_from(id)
                    .map(PlayerId)
                    .map_err(serde::de::Error::custom)
            }

            fn visit_str<E>(self, id: &str) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                id.parse().map(PlayerId).map_err(serde::de::Error::custom)
            }
        }

        deserializer.deserialize_any(PlayerIdVisitor)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, TypeScriptify)]
pub struct GameSettings {
    goal: u8,
}

#[derive(Debug, Clone, Serialize, Deserialize, TypeScriptify)]
pub struct Player {
    id: PlayerId,
    score: u8,
    hand: Vec<Card>,
    tricks: u8,
    taken: Vec<Card>,
}

#[derive(Debug, Clone, Serialize, Deserialize, TypeScriptify)]
pub struct GameState {
    players: [Player; 2],
    dealer: PlayerId,
    deck: Vec<Card>,
    decree: Card,
    played_cards: Vec<Card>,
    phase: Phase,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, TypeScriptify)]
#[serde(tag = "t")]
pub enum Phase {
    Lead {
        actor: PlayerId,
    },
    Follow {
        actor: PlayerId,
    },
    Exchanging {
        actor: PlayerId,
    },

    Discarding {
        drawn_card: Card,
        actor: PlayerId,
    },

    TrickComplete {
        leader: PlayerId,
        winner: PlayerId,
        next_leader: PlayerId,
        ready: HashSet<PlayerId>,
    },
    RoundComplete {
        ready: HashSet<PlayerId>,

        #[ts(ts_type = "{ [playerId: number]: number }")]
        points: HashMap<PlayerId, u8>,
    },
    GameComplete {
        winner: PlayerId,
    },
}

#[cfg(test)]
mod phase_test {
    use super::*;

    #[test]
    fn round_complete_serde() {
        let mut ready: HashSet<PlayerId> = HashSet::new();
        ready.insert(PlayerId(1));
        ready.insert(PlayerId(2));

        let mut points: HashMap<PlayerId, u8> = HashMap::new();
        points.insert(PlayerId(1), 5);
        points.insert(PlayerId(2), 10);

        let phase = Phase::RoundComplete { ready, points };
        let json = serde_json::json!({
            "t": "RoundComplete",
            "ready": [1,2],
            "points": {
                "1": 5,
                "2": 10
            }
        });

        assert_eq!(&serde_json::from_value::<Phase>(json).unwrap(), &phase);

        assert_eq!(
            serde_json::from_value::<Phase>(serde_json::to_value(&phase).unwrap()).unwrap(),
            phase
        );
    }
}

impl GameState {
    pub fn is_game_completed(&self) -> bool {
        matches!(self.phase, Phase::GameComplete { .. })
    }
    pub fn create(players: &[PlayerId], _settings: &GameSettings, seed: i64) -> Option<Self> {
        match *players {
            [player1_id, player2_id] => {
                let seed = seed.unsigned_abs();
                Some(GameState::new(player1_id, player2_id, seed))
            }
            _ => None,
        }
    }
    pub fn new(player1_id: PlayerId, player2_id: PlayerId, seed: u64) -> Self {
        let mut rng = rand_pcg::Pcg32::seed_from_u64(seed);

        let mut deck = Card::cards();
        deck.shuffle(&mut rng);

        let decree = deck.pop().unwrap();

        let player1 = Player {
            id: player1_id,
            score: 0,
            hand: deck.drain(0..13).collect::<Vec<_>>(),
            tricks: 0,
            taken: vec![],
        };

        let player2 = Player {
            id: player2_id,
            score: 0,
            hand: deck.drain(0..13).collect::<Vec<_>>(),
            tricks: 0,
            taken: vec![],
        };

        let mut players = [player1, player2];
        players.shuffle(&mut rng);
        players
            .iter_mut()
            .for_each(|player| player.hand.sort_unstable());

        let dealer = players[0].id;

        let phase = Phase::Lead {
            actor: players[0].id,
        };

        GameState {
            players,
            dealer,
            deck,
            decree,
            played_cards: Vec::new(),
            phase,
        }
    }

    fn prepare_next_round(&mut self, mut rng: &mut impl rand::Rng) {
        self.deck = Card::cards();
        self.deck.shuffle(&mut rng);
        self.decree = self.deck.pop().unwrap();

        for player in self.players.iter_mut() {
            player.tricks = 0;
            player.taken.clear();
            player.hand = self.deck.drain(0..13).collect::<Vec<_>>();
            player.hand.sort_unstable();
        }

        self.played_cards.clear();

        let previous_dealer = self.dealer;
        self.dealer = self
            .players
            .iter()
            .find(|p| p.id != previous_dealer)
            .unwrap()
            .id;

        self.phase = Phase::Lead { actor: self.dealer };
    }

    pub fn can_perform_action(
        &self,
        _settings: &GameSettings,
        user_id: PlayerId,
        action: &Action,
    ) -> Option<bool> {
        let user = match self.get_player(user_id) {
            Some(player) => player,
            None => return Some(false),
        };
        let result = match (&self.phase, action) {
            (&Phase::Lead { actor }, &Action::PlayCard { idx }) => {
                actor == user_id && user.hand.get(idx).is_some()
            }
            (Phase::Lead { .. }, _) => false,

            (Phase::Follow { actor }, &Action::PlayCard { idx }) => {
                if *actor != user_id {
                    return Some(false);
                }

                let led_card = self.played_cards.get(0)?;
                let played_card = *user.hand.get(idx)?;

                let highest_card_of_led_suit_opt = user
                    .hand
                    .iter()
                    .filter(|c| c.suit() == led_card.suit())
                    .max()
                    .copied();

                if let Some(highest_card) = highest_card_of_led_suit_opt {
                    played_card.suit() == led_card.suit()
                        && if led_card.rank() == Rank::Eleven {
                            played_card.rank() == highest_card.rank()
                                || played_card.rank() == Rank::One
                        } else {
                            true
                        }
                } else {
                    true
                }
            }
            (Phase::Follow { .. }, _) => false,

            (&Phase::Exchanging { actor, .. }, &Action::Exchange { idx }) => {
                actor == user_id
                    && if let Some(idx) = idx {
                        user.hand.get(idx).is_some()
                    } else {
                        true
                    }
            }
            (Phase::Exchanging { .. }, _) => false,

            (&Phase::Discarding { actor, .. }, &Action::Discard { idx }) => {
                actor == user_id && user.hand.get(idx).is_some()
            }
            (&Phase::Discarding { .. }, _) => false,

            (Phase::TrickComplete { ready, .. }, Action::Ready) => !ready.contains(&user.id),
            (Phase::TrickComplete { .. }, _) => false,

            (Phase::RoundComplete { ready, .. }, Action::Ready) => !ready.contains(&user.id),
            (Phase::RoundComplete { .. }, _) => false,

            (Phase::GameComplete { .. }, _) => false,
        };
        Some(result)
    }

    pub fn perform_action(
        &self,
        settings: &GameSettings,
        actor_id: PlayerId,
        action: &Action,
        seed: i64,
    ) -> Option<Self> {
        if !self
            .can_perform_action(settings, actor_id, action)
            .unwrap_or(false)
        {
            return None;
        }
        let mut rng = rand_pcg::Pcg32::seed_from_u64(seed.unsigned_abs());
        let mut result = self.clone();

        match (&self.phase, action) {
            (Phase::Lead { .. }, &Action::PlayCard { idx }) => {
                let actor = result.get_player_mut(actor_id)?;
                let card = actor.hand.remove(idx);
                result.played_cards.push(card);

                let other_player = result.get_other_player_mut(actor_id)?;

                if card.rank() == Rank::Three {
                    result.phase = Phase::Exchanging { actor: actor_id }
                } else if card.rank() == Rank::Five {
                    let drawn_card = result.deck.pop()?;
                    let actor = result.get_player_mut(actor_id)?;
                    actor.hand.push(drawn_card);

                    result.phase = Phase::Discarding {
                        actor: actor_id,
                        drawn_card,
                    }
                } else {
                    result.phase = Phase::Follow {
                        actor: other_player.id,
                    }
                }
            }
            (Phase::Lead { .. }, _) => return None,

            (Phase::Follow { .. }, &Action::PlayCard { idx }) => {
                let actor = result.get_player_mut(actor_id)?;
                let previous_follower_id = actor.id;
                let followed_card = actor.hand.remove(idx);

                result.played_cards.push(followed_card);

                if followed_card.rank() == Rank::Three {
                    result.phase = Phase::Exchanging { actor: actor_id }
                } else if followed_card.rank() == Rank::Five {
                    let drawn_card = result.deck.pop()?;
                    let actor = result.get_player_mut(actor_id)?;
                    actor.hand.push(drawn_card);
                    result.phase = Phase::Discarding {
                        actor: actor_id,
                        drawn_card,
                    }
                } else {
                    let other_player = result.get_other_player(actor_id)?;
                    let previous_leader_id = other_player.id;

                    let TrickResult {
                        next_leader,
                        winner,
                    } = TrickResult::new(
                        &result.played_cards,
                        previous_leader_id,
                        previous_follower_id,
                        result.decree,
                    );

                    result.phase = Phase::TrickComplete {
                        leader: previous_leader_id,
                        next_leader,
                        winner,
                        ready: HashSet::new(),
                    }
                }
            }
            (Phase::Follow { .. }, _) => return None,

            (Phase::Exchanging { .. }, Action::Exchange { idx }) => {
                if let Some(idx) = idx {
                    let decree = result.decree;
                    let player = result.get_player_mut(actor_id)?;
                    let card = player.hand.remove(*idx);
                    player.hand.push(decree);
                    result.decree = card;
                }

                let other_player = result.get_other_player(actor_id)?;
                if result.played_cards.len() < 2 {
                    // current actor was leading
                    result.phase = Phase::Follow {
                        actor: other_player.id,
                    }
                } else {
                    // current actor was following
                    let TrickResult {
                        next_leader,
                        winner,
                    } = TrickResult::new(
                        &result.played_cards,
                        other_player.id,
                        actor_id,
                        result.decree,
                    );

                    result.phase = Phase::TrickComplete {
                        leader: actor_id,
                        next_leader,
                        winner,
                        ready: HashSet::new(),
                    }
                }
            }
            (Phase::Exchanging { .. }, _) => return None,

            (Phase::Discarding { .. }, Action::Discard { idx }) => {
                let selected_card = {
                    let player = result.get_player_mut(actor_id)?;
                    player.hand.remove(*idx)
                };
                result.deck.insert(0, selected_card);

                let other_player = result.get_other_player(actor_id)?;
                if result.played_cards.len() < 2 {
                    // current actor was leading
                    result.phase = Phase::Follow {
                        actor: other_player.id,
                    }
                } else {
                    // current actor was following
                    let TrickResult {
                        next_leader,
                        winner,
                    } = TrickResult::new(
                        &result.played_cards,
                        other_player.id,
                        actor_id,
                        result.decree,
                    );

                    result.phase = Phase::TrickComplete {
                        leader: actor_id,
                        next_leader,
                        winner,
                        ready: HashSet::new(),
                    }
                }
            }
            (Phase::Discarding { .. }, _) => return None,

            (
                Phase::TrickComplete {
                    winner,
                    next_leader,
                    ..
                },
                Action::Ready,
            ) => {
                if let Phase::TrickComplete { ready, .. } = &mut result.phase {
                    ready.insert(actor_id);
                    if ready.len() < 2 {
                        return Some(result);
                    }
                }

                let treasures = result
                    .played_cards
                    .iter()
                    .filter(|c| c.rank() == Rank::Seven)
                    .count() as u8;
                let mut played_cards = result.played_cards.split_off(0);
                let winner_mut = result.get_player_mut(*winner)?;
                winner_mut.score += treasures;
                winner_mut.tricks += 1;
                winner_mut.taken.append(&mut played_cards);

                if result.players.iter().any(|p| p.hand.is_empty()) {
                    let points = result
                        .players
                        .iter()
                        .map(|player| {
                            let points: u8 = match player.tricks {
                                0..=3 => 6,
                                4 => 1,
                                5 => 2,
                                6 => 3,
                                7..=9 => 6,
                                _ => 0,
                            };
                            (player.id, points)
                        })
                        .collect::<HashMap<_, _>>();
                    result.phase = Phase::RoundComplete {
                        ready: HashSet::new(),
                        points,
                    }
                } else {
                    result.phase = Phase::Lead {
                        actor: *next_leader,
                    };
                }
            }
            (Phase::TrickComplete { .. }, _) => return None,

            (Phase::RoundComplete { points, .. }, Action::Ready) => {
                if let Phase::RoundComplete { ready, .. } = &mut result.phase {
                    ready.insert(actor_id);
                    if ready.len() < 2 {
                        return Some(result);
                    }
                }

                for (&player_id, &points) in points {
                    result.get_player_mut(player_id)?.score += points;
                }

                let point_leader = result
                    .players
                    .iter()
                    .filter(|p| p.score >= settings.goal)
                    .max_by(|a, b| a.score.cmp(&b.score));

                if let Some(point_leader) = point_leader {
                    let winner = if result.players.iter().all(|p| p.score == point_leader.score) {
                        let trick_leader =
                            result.players.iter().max_by(|a, b| a.tricks.cmp(&b.tricks));
                        trick_leader.unwrap()
                    } else {
                        point_leader
                    };

                    result.phase = Phase::GameComplete { winner: winner.id };
                } else {
                    result.prepare_next_round(&mut rng);
                }
            }
            (Phase::RoundComplete { .. }, _) => return None,
            (Phase::GameComplete { .. }, _) => return None,
        }

        result
            .players
            .iter_mut()
            .for_each(|player| player.hand.sort_unstable());

        Some(result)
    }

    fn get_player(&self, player_id: PlayerId) -> Option<&Player> {
        self.players.iter().find(|p| p.id == player_id)
    }
    fn get_player_mut(&mut self, player_id: PlayerId) -> Option<&mut Player> {
        self.players.iter_mut().find(|p| p.id == player_id)
    }
    fn get_other_player(&self, player_id: PlayerId) -> Option<&Player> {
        self.players.iter().find(|&p| p.id != player_id)
    }
    fn get_other_player_mut(&mut self, player_id: PlayerId) -> Option<&mut Player> {
        self.players.iter_mut().find(|p| p.id != player_id)
    }
}

#[derive(Debug, PartialEq, Deserialize, TypeScriptify)]
#[serde(tag = "t")]
pub enum Action {
    PlayCard { idx: usize },
    Exchange { idx: Option<usize> },
    Discard { idx: usize },
    Ready,
}

#[cfg(test)]
mod action_test {
    use super::*;
    use serde_json::*;

    #[test]
    fn deserialize_round_complete() {
        assert_eq!(
            from_value::<Action>(json!({ "t": "Ready" })).unwrap(),
            Action::Ready
        );
    }
}

struct TrickResult {
    winner: PlayerId,
    next_leader: PlayerId,
}
impl TrickResult {
    fn new(
        cards: &[Card],
        leading_player: PlayerId,
        following_player: PlayerId,
        decree: Card,
    ) -> Self {
        let mut led_card = *cards.get(0).unwrap();
        let mut followed_card = *cards.get(1).unwrap();

        if led_card.rank() == Rank::Nine && followed_card.rank() != Rank::Nine {
            led_card = Card::new(led_card.rank(), decree.suit());
        }

        if followed_card.rank() == Rank::Nine && led_card.rank() != Rank::Nine {
            followed_card = Card::new(followed_card.rank(), decree.suit());
        }

        let winner = if led_card.suit() == followed_card.suit() {
            if led_card.rank() > followed_card.rank() {
                leading_player
            } else {
                following_player
            }
        } else if led_card.suit() == decree.suit() {
            leading_player
        } else if followed_card.suit() == decree.suit() {
            following_player
        } else {
            leading_player
        };

        let next_leader = if following_player != winner && followed_card.rank() == Rank::One {
            following_player
        } else if leading_player != winner && led_card.rank() == Rank::One {
            leading_player
        } else {
            winner
        };

        Self {
            winner,
            next_leader,
        }
    }
}

#[derive(
    Debug,
    Eq,
    PartialEq,
    Ord,
    PartialOrd,
    Clone,
    Copy,
    Serialize_repr,
    Deserialize_repr,
    TypeScriptify,
)]
#[repr(u8)]
pub enum Card {
    Moons1 = 1,
    Moons2 = 2,
    Moons3 = 3,
    Moons4 = 4,
    Moons5 = 5,
    Moons6 = 6,
    Moons7 = 7,
    Moons8 = 8,
    Moons9 = 9,
    Moons10 = 10,
    Moons11 = 11,
    Keys1 = 12,
    Keys2 = 13,
    Keys3 = 14,
    Keys4 = 15,
    Keys5 = 16,
    Keys6 = 17,
    Keys7 = 18,
    Keys8 = 19,
    Keys9 = 20,
    Keys10 = 21,
    Keys11 = 22,
    Bells1 = 23,
    Bells2 = 24,
    Bells3 = 25,
    Bells4 = 26,
    Bells5 = 27,
    Bells6 = 28,
    Bells7 = 29,
    Bells8 = 30,
    Bells9 = 31,
    Bells10 = 32,
    Bells11 = 33,
}

impl Card {
    pub fn new(rank: Rank, suit: Suit) -> Self {
        match suit {
            Suit::Moons => match rank {
                Rank::One => Card::Moons1,
                Rank::Two => Card::Moons2,
                Rank::Three => Card::Moons3,
                Rank::Four => Card::Moons4,
                Rank::Five => Card::Moons5,
                Rank::Six => Card::Moons6,
                Rank::Seven => Card::Moons7,
                Rank::Eight => Card::Moons8,
                Rank::Nine => Card::Moons9,
                Rank::Ten => Card::Moons10,
                Rank::Eleven => Card::Moons11,
            },
            Suit::Keys => match rank {
                Rank::One => Card::Keys1,
                Rank::Two => Card::Keys2,
                Rank::Three => Card::Keys3,
                Rank::Four => Card::Keys4,
                Rank::Five => Card::Keys5,
                Rank::Six => Card::Keys6,
                Rank::Seven => Card::Keys7,
                Rank::Eight => Card::Keys8,
                Rank::Nine => Card::Keys9,
                Rank::Ten => Card::Keys10,
                Rank::Eleven => Card::Keys11,
            },
            Suit::Bells => match rank {
                Rank::One => Card::Bells1,
                Rank::Two => Card::Bells2,
                Rank::Three => Card::Bells3,
                Rank::Four => Card::Bells4,
                Rank::Five => Card::Bells5,
                Rank::Six => Card::Bells6,
                Rank::Seven => Card::Bells7,
                Rank::Eight => Card::Bells8,
                Rank::Nine => Card::Bells9,
                Rank::Ten => Card::Bells10,
                Rank::Eleven => Card::Bells11,
            },
        }
    }
    pub fn suit(&self) -> Suit {
        match self {
            Card::Moons1
            | Card::Moons2
            | Card::Moons3
            | Card::Moons4
            | Card::Moons5
            | Card::Moons6
            | Card::Moons7
            | Card::Moons8
            | Card::Moons9
            | Card::Moons10
            | Card::Moons11 => Suit::Moons,

            Card::Bells1
            | Card::Bells2
            | Card::Bells3
            | Card::Bells4
            | Card::Bells5
            | Card::Bells6
            | Card::Bells7
            | Card::Bells8
            | Card::Bells9
            | Card::Bells10
            | Card::Bells11 => Suit::Bells,

            Card::Keys1
            | Card::Keys2
            | Card::Keys3
            | Card::Keys4
            | Card::Keys5
            | Card::Keys6
            | Card::Keys7
            | Card::Keys8
            | Card::Keys9
            | Card::Keys10
            | Card::Keys11 => Suit::Keys,
        }
    }

    pub fn rank(&self) -> Rank {
        match self {
            Card::Bells1 | Card::Moons1 | Card::Keys1 => Rank::One,
            Card::Bells2 | Card::Moons2 | Card::Keys2 => Rank::Two,
            Card::Bells3 | Card::Moons3 | Card::Keys3 => Rank::Three,
            Card::Bells4 | Card::Moons4 | Card::Keys4 => Rank::Four,
            Card::Bells5 | Card::Moons5 | Card::Keys5 => Rank::Five,
            Card::Bells6 | Card::Moons6 | Card::Keys6 => Rank::Six,
            Card::Bells7 | Card::Moons7 | Card::Keys7 => Rank::Seven,
            Card::Bells8 | Card::Moons8 | Card::Keys8 => Rank::Eight,
            Card::Bells9 | Card::Moons9 | Card::Keys9 => Rank::Nine,
            Card::Bells10 | Card::Moons10 | Card::Keys10 => Rank::Ten,
            Card::Bells11 | Card::Moons11 | Card::Keys11 => Rank::Eleven,
        }
    }

    pub fn cards() -> Vec<Card> {
        vec![
            Card::Moons1,
            Card::Moons2,
            Card::Moons3,
            Card::Moons4,
            Card::Moons5,
            Card::Moons6,
            Card::Moons7,
            Card::Moons8,
            Card::Moons9,
            Card::Moons10,
            Card::Moons11,
            Card::Keys1,
            Card::Keys2,
            Card::Keys3,
            Card::Keys4,
            Card::Keys5,
            Card::Keys6,
            Card::Keys7,
            Card::Keys8,
            Card::Keys9,
            Card::Keys10,
            Card::Keys11,
            Card::Bells1,
            Card::Bells2,
            Card::Bells3,
            Card::Bells4,
            Card::Bells5,
            Card::Bells6,
            Card::Bells7,
            Card::Bells8,
            Card::Bells9,
            Card::Bells10,
            Card::Bells11,
        ]
    }
}

#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Clone, Copy, TypeScriptify)]
pub enum Suit {
    Moons,
    Keys,
    Bells,
}

impl Suit {
    pub fn suits() -> Vec<Suit> {
        vec![Suit::Keys, Suit::Bells, Suit::Moons]
    }
}

#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Clone, Copy, TypeScriptify)]
pub enum Rank {
    One = 1,
    Two = 2,
    Three = 3,
    Four = 4,
    Five = 5,
    Six = 6,
    Seven = 7,
    Eight = 8,
    Nine = 9,
    Ten = 10,
    Eleven = 11,
}

impl Rank {
    pub fn ranks() -> Vec<Rank> {
        vec![
            Rank::One,
            Rank::Two,
            Rank::Three,
            Rank::Four,
            Rank::Five,
            Rank::Six,
            Rank::Seven,
            Rank::Eight,
            Rank::Nine,
            Rank::Ten,
            Rank::Eleven,
        ]
    }
}

#[cfg(test)]
mod rank_tests {
    use super::*;

    #[test]
    fn rank_ord() {
        assert!(Rank::Eleven > Rank::Ten)
    }
}
