use lib::{Action, GameSettings, GameState, PlayerId};
use serde::Deserialize;
use serde::Serialize;

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct InitialStateReq {
    pub players: Vec<PlayerId>,
    pub settings: GameSettings,
    pub seed: i64,
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct InitialStateRes {
    pub state: GameState,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PerformActionReq {
    pub performed_by: PlayerId,
    pub action: Action,
    pub state: GameState,
    pub settings: GameSettings,
    pub seed: i64,
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct PerformActionRes {
    pub completed: bool,
    pub next_state: GameState,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct InfoReq {
    pub settings: GameSettings,
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct InfoRes {
    pub player_counts: Vec<usize>,
}
