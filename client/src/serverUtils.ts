export function getSuit(card: Card): Suit {
  if (card <= 11) {
    return "A";
  } else if (card >= 12 && card <= 22) {
    return "B";
  } else {
    return "C";
  }
}

export function getRank(card: Card): Rank {
  return (((card - 1) % 11) + 1).toString() as Rank;
}

export function getCard(rank: Rank, suit: Suit): Card {
  let rankNum = Number.parseInt(rank);
  switch (suit) {
    case "A":
      return rankNum as Card;
    case "B":
      return (rankNum + 11) as Card;
    case "C":
      return (rankNum + 22) as Card;
  }
}
