import { createEventDispatcher } from "svelte";

export function isThinking(phase: Phase, playerId: PlayerId): boolean {
  switch (phase.t) {
    case "Lead":
      return phase.actor === playerId;
    case "Follow":
      return phase.actor === playerId;
    case "Exchanging":
      return phase.actor === playerId;
    case "Discarding":
      return phase.actor === playerId;
    case "TrickComplete":
      return !phase.ready.includes(playerId);
    case "RoundComplete":
      return !phase.ready.includes(playerId);
    case "GameComplete":
      return false;
  }
}

export function canClickCard(phase: Phase, playerId: PlayerId): boolean {
  switch (phase.t) {
    case "Lead":
      return phase.actor === playerId;
    case "Follow":
      return phase.actor === playerId;
    case "Exchanging":
      return phase.actor === playerId;
    case "Discarding":
      return phase.actor === playerId;

    case "RoundComplete":
    case "TrickComplete":
    case "GameComplete":
      return false;
  }
}

export function createCardAction(phase: Phase, idx: number): Action | null {
  switch (phase.t) {
    case "Follow":
    case "Lead":
      return { t: "PlayCard", idx };
    case "Exchanging":
      return { t: "Exchange", idx };

    case "Discarding":
      return { t: "Discard", idx };

    case "RoundComplete":
    case "TrickComplete":
    case "GameComplete":
      return null;
  }
}

export function createActionDispatcher(): (action: Action) => void {
  const dispatch = createEventDispatcher();

  return (action: Action) => {
    dispatch("action", action);
  };
}
