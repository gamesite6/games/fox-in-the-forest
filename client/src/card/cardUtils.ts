import { getRank, getSuit } from "../serverUtils";

export function cardValue(c: Card): number {
  return parseInt(getRank(c));
}

export function rankTitle(rank: Rank): string {
  switch (rank) {
    case "11":
      return "Regent";
    case "9":
      return "Genie";
    case "7":
      return "Jewel";
    case "5":
      return "Hunter";
    case "3":
      return "Wolf";
    case "1":
      return "Dove";
    default:
      return "〜";
  }
}

export function cardTitle(c: Card): string {
  const rank = getRank(c);
  return rankTitle(rank);
}

export function suitEmoji(suit: Suit): string {
  switch (suit) {
    case "A":
      return "❄️";
    case "B":
      return "🌲";
    case "C":
      return "🔥";
  }
}

export function rankEmoji(rank: Rank): string | undefined {
  switch (rank) {
    case "1":
      return "🕊️";
    case "3":
      return "🐺";
    case "5":
      return "🧔";
    case "7":
      return "💎";
    case "9":
      return "🧞";
    case "11":
      return "👸🏼";
  }
}

export function suitClass(suit: Suit): string {
  switch (suit) {
    case "A":
      return "suit-a";
    case "B":
      return "suit-b";
    case "C":
      return "suit-c";
  }
}

export function max(cards: Card[], suit?: Suit): Card | undefined {
  if (suit) {
    cards = cards.filter((c) => getSuit(c) === suit);
  }

  if (cards.length === 0) {
    return;
  } else {
    return Math.max(...cards) as Card;
  }
}
