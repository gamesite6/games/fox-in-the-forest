import GameComponent from "./Game.svelte";
import ReferenceComponent from "./Reference.svelte";
import SettingsComponent from "./Settings.svelte";

type GameProps = {
  playerId?: PlayerId;
  state: GameState;
  settings: GameSettings;
};

export class Game extends HTMLElement {
  #playerId?: PlayerId;
  #state?: GameState;
  #settings?: GameSettings;
  #app?: GameComponent;

  set state(state: GameState) {
    if (this.#state !== state) {
      this.#state = state;
      this.rerender({ state });
    }
  }

  set settings(settings: GameSettings) {
    if (this.#settings !== settings) {
      this.#settings = settings;
      this.rerender({ settings });
    }
  }

  set playerid(playerId: PlayerId | undefined) {
    if (this.#playerId !== playerId) {
      this.#playerId = playerId;
      this.rerender({ playerId });
    }
  }

  connectedCallback() {
    if (this.#settings && this.#state) {
      this.#app = new GameComponent({
        target: this,
        props: {
          playerId: this.#playerId,
          settings: this.#settings,
          state: this.#state,
        },
      });

      this.#app.$on("action", (evt: CustomEvent) => {
        this.dispatchEvent(new CustomEvent("action", { detail: evt.detail }));
      });
    }
  }

  private rerender(changedProps: Partial<GameProps>) {
    if (this.#app) {
      this.#app.$set(changedProps);
    }
  }
}

export class Reference extends HTMLElement {
  _settings?: GameSettings;
  app?: ReferenceComponent;

  constructor() {
    super();
  }

  set settings(settings: GameSettings) {
    this._settings = settings;
    this.setAttribute("settings", JSON.stringify(settings));
  }

  connectedCallback() {
    this.app = new ReferenceComponent({
      target: this,
      props: {
        settings: this._settings,
      },
    });
  }

  static get observedAttributes() {
    return ["settings"];
  }

  attributeChangedCallback(name: string, oldValue: any, newValue: any) {
    if (this.app && oldValue !== newValue) {
      this.app.$set({
        settings: this._settings,
      });
    }
  }
}

export { defaultSettings, playerCounts } from "./settings";

type SettingsProps = {
  settings?: GameSettings;
  readonly?: boolean;
};

export class Settings extends HTMLElement {
  #app?: SettingsComponent;

  #settings?: GameSettings;

  set settings(settings: GameSettings) {
    if (this.#settings !== settings) {
      this.#settings = settings;
      this.rerender({ settings });
    }
  }

  get #readonly(): boolean {
    const attr = this.attributes.getNamedItem("readonly");
    return attr !== null && attr.value !== "false";
  }

  connectedCallback() {
    this.#app = new SettingsComponent({
      target: this,
      props: {
        readonly: this.#readonly,
        settings: this.#settings,
      },
    });

    this.#app.$on("settings_change", (evt: CustomEvent) => {
      this.dispatchEvent(
        new CustomEvent("settings_change", {
          detail: evt.detail,
        })
      );
    });
  }

  private rerender(changedProps: Partial<SettingsProps>) {
    if (this.#app) {
      this.#app.$set(changedProps);
    }
  }

  static get observedAttributes() {
    return ["readonly"];
  }

  attributeChangedCallback(name: string, oldValue: any, newValue: any) {
    if (name === "readonly") {
      this.rerender({ readonly: this.#readonly });
    }
  }
}
