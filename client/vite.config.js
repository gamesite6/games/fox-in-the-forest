import path from "path";
import { defineConfig } from "vite";
import { svelte } from "@sveltejs/vite-plugin-svelte";

// https://vitejs.dev/config/
export default defineConfig({
  base: "/static/wolf-in-the-woods/",
  build: {
    lib: {
      entry: path.resolve(__dirname, "src", "main.ts"),
      fileName: "wolf-in-the-woods",
      name: "Gamesite6_WolfInTheWoods",
    },
  },
  css: {
    modules: {},
  },
  plugins: [svelte()],
});
