type PlayerId = number;

type GameSettings = { goal: number };

type GameState = {
  players: Player[];
  dealer: PlayerId;
  deck: Card[];
  decree: Card;
  played_cards: Card[];
  phase: Phase;
};

type Player = {
  id: PlayerId;
  score: number;
  hand: Card[];
  tricks: number;
  taken: Card[];
};

type Card =
  | 1
  | 2
  | 3
  | 4
  | 5
  | 6
  | 7
  | 8
  | 9
  | 10
  | 11
  | 12
  | 13
  | 14
  | 15
  | 16
  | 17
  | 18
  | 19
  | 20
  | 21
  | 22
  | 23
  | 24
  | 25
  | 26
  | 27
  | 28
  | 29
  | 30
  | 31
  | 32
  | 33;

type Phase =
  | { t: "Lead"; actor: PlayerId }
  | { t: "Follow"; actor: PlayerId }
  | { t: "Exchanging"; actor: PlayerId }
  | { t: "Discarding"; drawn_card: Card; actor: PlayerId }
  | {
      t: "TrickComplete";
      leader: PlayerId;
      winner: PlayerId;
      next_leader: PlayerId;
      ready: PlayerId[];
    }
  | {
      t: "RoundComplete";
      ready: PlayerId[];
      points: { [playerId: number]: number };
    }
  | { t: "GameComplete"; winner: PlayerId };

type Action =
  | { t: "PlayCard"; idx: number }
  | { t: "Exchange"; idx: number | null }
  | { t: "Discard"; idx: number }
  | { t: "Ready" };
